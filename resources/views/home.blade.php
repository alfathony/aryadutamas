@extends('layouts.app')

@section('content')
<div>
    <div class="container">
        <div class="row justify-content-center no-gutters py-3">
            <div class="col-md-10">
                <div class="text-white text-center">
                    @if (request()->getHttpHost() === 'emas24investama.com' or request()->getHttpHost() === 'www.emas24investama.com')
                        <img src="{{ asset('images/logo/emas24.jpeg') }}" style="max-width: 360px; width: 100%">
                    @elseif (request()->getHttpHost() === 'dutamasmulia.com' or request()->getHttpHost() === 'www.dutamasmulia.com')
                        <img src="{{ asset('images/logo/dutamasmulia.jpeg') }}" style="max-width: 360px; width: 100%">
                    @else
                        <img src="{{ asset('images/logo/aryadutamas.jpeg') }}" style="max-width: 360px; width: 100%">
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="row bg-white py-3 rounded">
                    <div class="col-md-6">
                        <div class="owl-carousel">
                            @for ($i = 1; $i <= 24; $i++)
                                <img src="{{ asset('slides/'.$i.'.jpg') }}" alt="" class="img-fluid rounded">
                            @endfor
                        </div>
                        <div class="row" id="section-engage">
                            <div class="col-4 text-center text-md-left p-2 bg-light">
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('images/eye.png') }}" alt="" class="d-inline"></div>
                                    <div class="col-sm-9">
                                        <div>{{ $visitorToday }}</div>
                                        <div class="small d-inline">Dilihat hari ini</div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-engage {{ Session::get('actSubmitted') ? 'submitted' : '' }} col-4 text-center text-md-left p-2 bg-light">

                                @if (Session::get('actSubmitted'))
                                    <div class="row">
                                        @else
                                            <div class="row" onclick="submitLike(1)">
                                @endif
                                    <div class="col-sm-3"><img src="{{ asset('images/like.png') }}" alt="" class="d-inline"></div>
                                    <div class="col-sm-9">
                                        <div>{{ $likesToday }}</div>
                                        <div class="small d-inline">Disukai hari ini</div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-engage {{ Session::get('actSubmitted') ? 'submitted' : '' }} col-4 text-center text-md-left p-2 bg-light">
                                @if (Session::get('actSubmitted'))
                                    <div class="row">
                                        @else
                                            <div class="row" onclick="submitLike(0)">
                                                @endif
                                    <div class="col-sm-3"><img src="{{ asset('images/dislike.png') }}" alt="" class="d-inline"></div>
                                    <div class="col-sm-9">
                                        <div>{{ $dislikesToday }}</div>
                                        <div class="small d-inline">Tidak disukai</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-muted">Harga emas hari ini: {{ \Carbon\Carbon::now()->isoFormat('dddd, D MMM Y h:m') }}</div>
                        <hr>
                        <div class="mt-3">
                            <h4>Harga Emas Jual</h4>
                            <div class="font-weight-bolder h1">Rp {{ number_format(($price->price_sell * 99.5) / 100) }} <span class="h4">/ gram</span></div>
                        </div>
                        <hr>
                        <div>
                            <h4>Harga Emas Beli</h4>
                            <div class="font-weight-bolder h1">Rp {{ number_format(($price->price_buy * 98) / 100) }} <span class="h4">/ gram</span></div>
                        </div>

{{--                        <div class="mt-3">--}}
{{--                            <div class="text-muted">Terakhir diperbarui {{ \Carbon\Carbon::parse($price->created_at)->diffForHumans()  }}</div>--}}
{{--                        </div>--}}

                        <hr>

                        <div>
                            <h6>Rekomendasi Analisa Harga Emas Hari Ini:</h6>

                            @if ($price->sorting % 2 == 0)
                                <div class="mt-2" style="height: 32px; border-radius: 50px; background: #FE0201">
                                    <div style="width: {{ $price->recommendation_buy }}%; height: 100%; border-radius: 50px 0 0 50px; background: #FFC003"></div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-6">
                                        <div class="h4">{{ $price->recommendation_buy }}%</div>
                                        <div>Melakukan aksi beli</div>
                                    </div>
                                    <div class="col-6 text-right">
                                        <div class="h4">{{ 100 - $price->recommendation_buy }}%</div>
                                        <div>Melakukan aksi jual</div>
                                    </div>
                                </div>
                            @else
                                <div class="mt-2" style="height: 32px; border-radius: 50px; background: #FFC003">
                                    <div style="width: {{ $price->recommendation_sell }}%; height: 100%; border-radius: 50px 0 0 50px; background: #FE0201"></div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-6">
                                        <div class="h4">{{ $price->recommendation_sell }}%</div>
                                        <div>Melakukan aksi jual</div>
                                    </div>
                                    <div class="col-6 text-right">
                                        <div class="h4">{{ 100 - $price->recommendation_sell }}%</div>
                                        <div>Melakukan aksi beli</div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="text-white text-center mt-4 mb-5">
                    <p>Present the product:</p>
                    <div class="d-inline-block p-3 rounded mr-1 ml-1 mb-3 bg-light">
                        <img src="{{ asset('images/logo-ubs.png') }}" alt="" style="max-width: 100px">
                    </div>
                    <div class="d-inline-block p-3 rounded mr-1 ml-1 mb-3 bg-light">
                        <img src="{{ asset('images/logo-ubs-l.png') }}" alt="" style="max-width: 100px">
                    </div>
                    <div class="d-inline-block p-3 rounded mr-1 ml-1 mb-3 bg-light">
                        <img src="{{ asset('images/logo-antam.png') }}" alt="" style="max-width: 100px">
                    </div>
                    <div class="d-inline-block p-3 rounded mr-1 ml-1 mb-3 bg-light">
                        <img src="{{ asset('images/logo-logammulia.png') }}" alt="" style="max-width: 100px">
                    </div>
                </div>
                <div class="text-white text-center mt-4 mb-5">
                    <p>Bagikan:</p>
                    <div class="fb-share-button" data-href="https://aryadutamas.com" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Faryadutamas.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                    <a href="whatsapp://send?text=Harga Emas dan Analisa Harga Emas Hari Ini - Arya Duta Mas {{ url()->current() }}"       data-action="share/whatsapp/share"
                       target="_blank" class="btn btn-whatsapp"><i class="fab fa-whatsapp"></i> Whatsapp</a>
                </div>
                <div class="text-white text-center mt-4">
                    <h5>Member Of</h5>
                    <p>PT. DutaMasMulia <br> Jln Prof Dr Satrio, Mall Ambasador Kuningan lantai dasar blok G-3A
                        <br> Telp 021 5793 3624</p>
                    <p>
                        <a href="https://DutaMasMulia.com" target="_blank" class="mr-2" style="color: #FFA205">www.DutaMasMulia.com </a>
                        <a href="https://AryaDutaMas.com" target="_blank" class="mr-2"  style="color: #FFA205">www.AryaDutaMas.com </a>
                        <a href="https://emas24investama.com" target="_blank" class="mr-2"  style="color: #FFA205">www.emas24investama.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        /**
         * sends a request to the specified url from a form. this will change the window location.
         * @param  {string} path the path to send the post request to
         * @param  {object} params the paramiters to add to the url
         * @param  {string} [method=post] the method to use on the form
         */

        function post(path, params, method='post') {

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            const form = document.createElement('form');
            form.method = method;
            form.action = path;

            for (const key in params) {
                if (params.hasOwnProperty(key)) {
                    const hiddenField = document.createElement('input');
                    hiddenField.type = 'hidden';
                    hiddenField.name = key;
                    hiddenField.value = params[key];

                    form.appendChild(hiddenField);
                }
            }

            document.body.appendChild(form);
            form.submit();
        }

        function submitLike(value){
            const params = {
                value: value,
            }
            return post('/', params);
        }
    </script>
    @endpush
