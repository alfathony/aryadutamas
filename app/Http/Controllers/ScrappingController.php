<?php

namespace App\Http\Controllers;

use App\GoldPriceLogModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;
use Sunra\PhpSimple\HtmlDomParser;

class ScrappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        setlocale(LC_TIME, 'id_ID');
        Carbon::setLocale('id');

        $url = "https://www.lakuemas.com/";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, "cookie.txt"); // save data cookie
        curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt"); // save data cookie
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        $dom = HtmlDomParser::str_get_html($response);

        $harga = array();

        foreach ($dom->find('div.box-harga') as $boxHarga){
            $item['harga_jual_emas'] = $this->filterDigitOnly($boxHarga->find('div div div.border-right h3', 0)->plaintext);
            $item['harga_beli_emas'] = $this->filterDigitOnly($boxHarga->find('div div div.border-left h3', 0)->plaintext);
            $harga = $item;
        }

        // check latest data
        $checkLatest = GoldPriceLogModel::orderBy('created_at', 'DESC')->first();

        $rekomendasi_beli = array(67,74,74,96,96,69,69,79,79,84,84,67);
        $rekomendasi_jual = array(85,85,68,68,58,58,93,93,68,68,59,59);

        if ($checkLatest){
            if ($checkLatest->sorting < 11){
                $nomorUrut = $checkLatest->sorting + 1;
            }else{
                $nomorUrut = 0; // balikin lagi ke 0
            }
        }else{
            $nomorUrut = 0;
        }

        $priceGold = new GoldPriceLogModel();
        $priceGold->sorting = $nomorUrut;
        $priceGold->price_sell = $harga['harga_jual_emas'];
        $priceGold->recommendation_sell = $rekomendasi_jual[$nomorUrut];
        $priceGold->price_buy = $harga['harga_beli_emas'];
        $priceGold->recommendation_buy = $rekomendasi_beli[$nomorUrut];

        try {
            $priceGold->save();
            return response()->json($harga);
        }catch (Exception $e){
            Log::debug($e);
            return $e;
        }

    }

    public function filterDigitOnly($str){
        $int = (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);
        return $int;
    }
}
