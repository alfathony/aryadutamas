<?php

namespace App\Http\Controllers;

use App\GoldPriceLogModel;
use App\LikesModel;
use App\VisitorModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dailyData = $this->dailyData();
        $getAgent = $this->getAgent();

        $countVisitorToday = VisitorModel::whereDate('created_at', Carbon::today())->count();
        $visitorToday = number_format($dailyData['viewers'] + $countVisitorToday,0,"",".");

        $countLikesToday = LikesModel::whereDate('created_at', Carbon::today())->where('like', 1)->count();
        $likesToday = number_format($dailyData['likes'] + $countLikesToday,0,"",".");

        $countDislikesToday = LikesModel::whereDate('created_at', Carbon::today())->where('dislike', 1)->count();
        $dislikesToday = number_format($dailyData['dislikes'] + $countDislikesToday,0,"",".");

        $price = GoldPriceLogModel::orderBy('created_at', 'DESC')->first();
        return view('home', compact('price', 'dailyData', 'getAgent', 'visitorToday', 'likesToday', 'dislikesToday'));
    }

    function dailyData(){
        // Data daily visitor, likes and dislike
        $dailyData = array(
            'Mon' => ['likes' => 1837478, 'dislikes' => 334, 'viewers' => 3753976], // Senin
            'Tue' => ['likes' => 1693754, 'dislikes' => 648, 'viewers' => 4593567], // Selasa
            'Wed' => ['likes' => 1458624, 'dislikes' => 676, 'viewers' => 3649592], // Rabu
            'Thu' => ['likes' => 1935837, 'dislikes' => 578, 'viewers' => 2384947], // Kamis
            'Fri' => ['likes' => 1634943, 'dislikes' => 378, 'viewers' => 1843682], // Jumat
            'Sat' => ['likes' => 1743348, 'dislikes' => 946, 'viewers' => 1779484], // Sabtu
            'Sun' => ['likes' => 1394638, 'dislikes' => 663, 'viewers' => 1479367], // Minggu
        );

        // Today
        $today = date('D');

        return $dailyData[$today];
    }

    function getAgent(){
        // Agent
        $agent = new Agent();

        $catchAgent = [
            'platform' => $agent->platform(),
            'browser' => $agent->browser(),
            'device' => $agent->device(),
            'ip' => \request()->ip(),
        ];

        $storeVisitor = new VisitorModel();
        $storeVisitor->platform = $agent->platform();
        $storeVisitor->browser = $agent->browser();
        $storeVisitor->device = $agent->device();
        $storeVisitor->ip = \request()->ip();
        $storeVisitor->save();

        return $catchAgent;
    }

    public function like(Request $request)
    {
        $storeLikeLog = new LikesModel();

        if ($request->value == 1){
            $storeLikeLog->like = 1;
        }else{
            $storeLikeLog->dislike = 1;
        }

        $storeLikeLog->save();

        $request->session()->flash('actSubmitted', true);

        return redirect()->back();
    }
}
