<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoldPriceLogModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gold_price_log_models', function (Blueprint $table) {
            $table->id();
            $table->integer('sorting')->default(0);
            $table->integer('price_buy')->nullable();
            $table->integer('recommendation_buy')->nullable();
            $table->integer('price_sell')->nullable();
            $table->integer('recommendation_sell')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gold_price_log_models');
    }
}
